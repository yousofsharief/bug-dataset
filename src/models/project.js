class Project {
    constructor(capitalName, numberOfBugs, specialRepoName) {
        this.capitalName = capitalName
        this.numberOfBugs = numberOfBugs
        this.specialRepoName = specialRepoName
        this.innerFolderName = this.specialRepoName ? this.specialRepoName : this.capitalName.toLowerCase()
        this.outerFolderName = `${this.innerFolderName}_container`
    }
}

module.exports = {
    Project
}