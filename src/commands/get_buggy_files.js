const shell = require('shelljs');
const projects = require('../config/projects_config').projects
const folderNamesConfig = require('../config/folder_names_config')

for (const project of projects) {
    for (let bugNumber = 1; bugNumber <= project.numberOfBugs; bugNumber++) {
        const dir = `${folderNamesConfig.projectsSourceFilesFolder}/${project.outerFolderName}/${project.innerFolderName}/.git`
        const str = getAffectedFiles(dir, bugNumber)
        const filePaths = str.split('\n')
        filePaths.pop()
        filePaths.forEach((path, index) => {
            shell.exec(`git --git-dir ${dir} show tags/Bug-${bugNumber}-full:${path} > ${folderNamesConfig.fixedFilesfolder}/${project.innerFolderName}_${bugNumber}_${index}.js`)
            shell.exec(`git --git-dir ${dir} show tags/Bug-${bugNumber}:${path} > ${folderNamesConfig.buggedFilesfolder}/${project.innerFolderName}_${bugNumber}_${index}.js`)
        })
    }    
}



function getAffectedFiles(dir, bugNumber) {
    return shell.exec(`git --git-dir ${dir} diff-tree --no-commit-id --name-only -r tags/Bug-${bugNumber}-full`).stdout
}