const shell = require('shelljs');
const config = require('../config/projects_config')
const folderNamesConfig = require('../config/folder_names_config')

for (const project of config.projects) {    
    for (let bugNumber = 1; bugNumber <= project.numberOfBugs; bugNumber++) {
        const diffFileName = `${project.innerFolderName}_${bugNumber}_diff.txt`
        shell.exec(`git --git-dir ${folderNamesConfig.projectsSourceFilesFolder}/${project.outerFolderName}/${project.innerFolderName}/.git diff tags/Bug-${bugNumber} tags/Bug-${bugNumber}-full > diffs/${diffFileName}`)
    }
}
