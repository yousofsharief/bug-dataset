const shell = require('shelljs');
const config = require('../config/projects_config')
const folderNamesConfig = require('../config/folder_names_config')


for (const project of config.projects) {
    if (!isThere(`${folderNamesConfig.projectsSourceFilesFolder}/${project.outerFolderName}`)) {
        shell.exec(`python main.py -p ${project.capitalName} -b 1 -t checkout -v fixed -o ${folderNamesConfig.projectsSourceFilesFolder}/${project.outerFolderName}`)
    }
}

function isThere(name) {
    return shell.ls(name).code !== 2
}
