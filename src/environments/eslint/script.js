const shell = require('shelljs')
const folderNamesConfig = require('../../config/folder_names_config')

shell.exec(`eslint ${folderNamesConfig.buggedFilesfolder}/**  --config src/environments/eslint/.eslintrc.json> ${folderNamesConfig.toolsReportsFolder}/eslint.txt`)