const Project = require('../models/project').Project

module.exports = {
    projects: [
        new Project('Bower', 3),
        new Project('Eslint', 333),
        new Project('Express', 27),
        new Project('Hessian.js', 9),
        new Project('Hexo', 12),
        new Project('Karma', 22),
        new Project('Mongoose', 29),
        new Project('Node-redis', 7, 'node_redis'),
        new Project('Pencilblue', 7),
        new Project('Shields', 4),
    ],
}

