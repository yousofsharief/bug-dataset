module.exports = {
    projectsSourceFilesFolder: 'projects_source_files',
    buggedFilesfolder: 'bugged_files',
    fixedFilesfolder: 'fixed_files',
    toolsReportsFolder: 'tool_reports'
}